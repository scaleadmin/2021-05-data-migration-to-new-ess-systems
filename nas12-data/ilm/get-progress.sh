
# 2021-06-07@18:14:28.186
# [I] 2021-06-07@17:00:01.534 Policy execution. 7484986 files dispatched.
# [I] 2021-06-07@18:00:13.181 Policy execution. 13232546 files dispatched.
for m in 06 
do
for i in {1..15}
do
  for n in {0..23}
  do
    h=$(printf %02i $n) #hour
    d=$(printf %02i $i) #day

    # echo "$m-$d@$h:"
    grep -m 1 "$m-$d@$h:" $1 | grep execution  | tr -d '()'
  done
done
done | awk -v last=0 '// { delta=$5-last;
                           printf "%s  %9i %9i inodes/hour\n",$5,$6,delta;
                           last=$5;
                          }'
