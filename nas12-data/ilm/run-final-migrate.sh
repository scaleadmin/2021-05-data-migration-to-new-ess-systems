#!/usr/bin/bash
base=/fs1201/id_sd_nas12_info/2021-05-data-migration-to-new-ess-systems/nas12-data/ilm
policy_file=$base/migrate-to-Data.policy

nodes=nas12io12,nas12io10,nas12io11
l=1

rm -f /tmp/migrate-to-Data.policy
cp $policy_file /tmp/

d="$1"
  echo ==  START $d $(date)
  cat /tmp/migrate-to-Data.policy
  set -x
  mmapplypolicy "$d" \
      -I yes \
      -L $l \
      -N $nodes \
      -a 4 \
      -P /tmp/migrate-to-Data.policy \
      --qos maintenance
  if [ $? -eq 0 ]; then
    echo "$d" SUCCESS 
  fi

  set +x
  echo == STOP $d $(date)
