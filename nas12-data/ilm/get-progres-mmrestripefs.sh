#  98.50 % complete on Thu Jun 10 14:06:18 2021  ( 480418207 inodes with total 1162213788 MB data processed)
#    0.12 % complete on Tue Jun  8 12:19:49 2021  (    607147 inodes with total    1423809 MB data processed)
for m in Jun
do
for d in {1..15}
do
  for n in {0..23}
  do
    h=$(printf %02i $n)
    d=$(printf %2i $d)
    grep 'inodes with total' $1 |grep -m 1 "$m $d $h:" | tr -d '()'
  done
done
done | awk -v lasti=0 -v lastd=0 '// { deltai=$10-lasti; deltad=$14-lastd;
                          printf "%s %s %s %s %9i %9i inodes/hour %6i MB/hour %3i%% \n",$6,$7,$8,$9,$10,deltai,deltad,$14*100/(3432274000-2261639504);
                          lasti=$10; lastd=$14;
                          }'

#   0.19 % complete on Tue Jun  8 12:24:25 2021  (   1012939 inodes with total    2183124 MB data processed)
#   0.19 % complete on Tue Jun  8 12:24:45 2021  (   1047778 inodes with total    2231166 MB data processed)
#   1    2  3       4  5   6    7  8        9          10      11    12   13      14
