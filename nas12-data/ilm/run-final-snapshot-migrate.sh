#!/usr/bin/bash
base=/fs1201/id_sd_nas12_info/2021-05-data-migration-to-new-ess-systems/nas12-data/ilm
policy_file=$base/migrate-to-Data.policy

nodes=nas12io12,nas12io10,nas12io11
l=2
snap='@GMT-2021.06.03-10.45.39'

rm -f /tmp/migrate-to-Data.policy
cp $policy_file /tmp/

d="$1"
  echo ==  START $d $(date)
  cat /tmp/migrate-to-Data.policy
  set -x
  mmapplypolicy "$d" \
      -I yes \
      -L $l \
      -N $nodes \
      -a 4 \
      -P /tmp/migrate-to-Data.policy \
      -S "$snap" \
      --qos maintenance
  if [ $? -eq 0 ]; then
    echo "$d" SUCCESS 
  fi

  set +x
  echo == STOP $d $(date)

# Snapshots in file system fs1202:
# Directory                SnapId    Status  Created                   Fileset
# @GMT-2021.06.03-10.45.39 6435      Valid   Thu Jun  3 12:45:44 2021  
# @GMT-2021.06.04-10.45.39 6437      Valid   Fri Jun  4 12:45:44 2021  
# @GMT-2021.06.05-10.45.39 6439      Valid   Sat Jun  5 12:45:45 2021  
# @GMT-2021.06.06-10.45.39 6441      Valid   Sun Jun  6 12:45:45 2021  
# @GMT-2021.06.07-10.45.39 6443      Valid   Mon Jun  7 12:45:44 2021  
# @GMT-2021.06.08-10.45.39 6445      Valid   Tue Jun  8 12:45:52 2021  
# @GMT-2021.06.09-10.45.39 6447      Valid   Wed Jun  9 12:45:47 2021  
# @GMT-2021.06.10-10.45.40 6449      Valid   Thu Jun 10 12:45:47 2021  
# @GMT-2021.06.11-10.45.40 6451      Valid   Fri Jun 11 12:45:46 2021  
