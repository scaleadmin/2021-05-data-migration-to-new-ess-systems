for m in  Jun
do
for i in {1..15}
do
  for n in {0..23}
  do
    h=$(printf %02i $n)
    grep -m 1 "$m  $i $h:" $1 | tr -d '()'
  done
done
done | awk -v last=0 '// { delta=$10-last;
                          printf "%s %s %s %s %9i %9i inodes/hour\n",$6,$7,$8,$9,$10,delta;
                          last=$10;
                          }'
