for vs in  \
  VS015_fs2201_io12_m_1 \
  VS064_fs2202_io12_m_1 \
  VS065_fs2203_io12_m_1 \
  VS066_fs3101h_io12_m_1 \
  VS067_fscesroot_io12 \
  VS080_protectFilePool_io08
do
  echo echo $vs
  echo mmvdisk vs delete --vs $vs
  echo mmvdisk vs undefine --vs $vs
done
