#!/usr/bin/bash
set -u

readonly cluster=nas22
readonly default_io_pair=io12
readonly default_rg=rg_${cluster}$default_io_pair

create-metadata-vs()
{
 if [ $# -lt 2 ]; then
   echo missing arguments fs vs-size [rg iopair]
 fi
 local fs=$1
 local vs_size=$2
 
 local rg
 local io_pair
 if [ $# -ge 4 ]; then
   rg="$3"
   io_pair="$4"
 else
   rg="$default_rg"
   io_pair="$default_io_pair"
 fi
 readonly rg io_pair
  
 echo "# $fs $vs_size $rg $io_pair"

 local vs_name="${fs}_${io_pair}_m_1"
 echo mmvdisk vs define --vs $vs_name --rg $rg --code 3WayReplication --block-size 1m --set-size  "$vs_size" --nsd-usage metadataOnly
 echo mmvdisk vs create --vs $vs_name
 echo
}

 create-metadata-vs fs2201  8000G
 create-metadata-vs fs2202  5000G
 create-metadata-vs fs2203   500G
 create-metadata-vs fs3101h 6000G
